from src.page_objects.pages.base_page import BasePage
from time import sleep


class HeaderPageElement(BasePage):
    
    def __init__(self):
        
        self.CONTAINER_MAIN_MENU = (self.By.XPATH, "/html/body/div[2]/div/div[1]/div/div[1]/div[2]/div/div[1]/div[2]/div/div[1]/ul")
        self.CONTAINER_SUB_MENU = (self.By.XPATH, "/html/body/div[2]/div/div[1]/div/div[1]/div[2]/div/div[1]/div[2]/div/div[1]/ul/li[1]/div/div[2]/ul")
        self.BTN_SEARCH = (self.By.XPATH, "/html/body/div[2]/div/div[1]/div/div[1]/div[2]/div/div[1]/div[2]/div/div[2]/ul[2]/li[3]/a")
        self.SEARCH_INPUT = (self.By.ID, "downshift-1-input")
  
        
class HeaderMenu(HeaderPageElement):

    def __init__(self, driver, page_elements):
        self.driver = driver
        self.page_elements = page_elements
                
                
    def access_smartphone_menu(self, label_link):
        BasePage.find_link(self, label_link).click()

        
    def search_by_model(self, model):
        BasePage.click(self, self.page_elements.BTN_SEARCH)
        BasePage.set_text(self, self.page_elements.SEARCH_INPUT, model)
        sleep(3)
        
