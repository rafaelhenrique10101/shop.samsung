from src.page_objects.page_elements.smartphone_page_elements import Sort
from src.page_objects.page_elements.smartphone_page_elements import SmartphonePageElement
from src.page_objects.page_elements.smartphone_page_elements import SmartphoneList 

class SmartphonePage:
    
    def __init__(self, driver):
        self.driver = driver
        self.page_element = SmartphonePageElement()
        self.sort = Sort(self.driver, self.page_element)
        self.smartphone_list = SmartphoneList(self.driver, self.page_element)
