from selenium import webdriver


def before_all(context):
    
    context.base_url = context.config.userdata['base_url']
    
        
def before_scenario(context, scenario):
        
    context.driver = webdriver.Chrome()
    
    context.driver.implicitly_wait(30)
    context.driver.set_page_load_timeout(60)
    context.driver.maximize_window()


def after_scenario(context, scenario):
    
    context.driver.quit()
