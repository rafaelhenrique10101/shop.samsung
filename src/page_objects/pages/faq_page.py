from src.page_objects.page_elements.faq_page_elements import FAQFlow
from src.page_objects.page_elements.faq_page_elements import FAQPageElement

class FAQPage:
    
    def __init__(self, driver):
        self.driver = driver
        self.page_elements = FAQPageElement()
        self.faq_flow = FAQFlow(self.driver, self.page_elements)
