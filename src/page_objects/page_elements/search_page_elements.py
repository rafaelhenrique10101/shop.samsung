from src.page_objects.pages.base_page import BasePage
from time import sleep


class SearchPageElement(BasePage):
    
    def __init__(self):
                
        self.CONTAINER_LIST_RESULT_SMARTPHONES = (self.By.CSS_SELECTOR, "section.vtex-search-1-x-tileList ul li")
  
        
class ListResultSmartphones(SearchPageElement):
        
    def __init__(self, driver, page_elements):
        self.driver = driver
        self.page_elements = page_elements
        
        
    def select_smartphone(self, model):
        container_list_smartphones = BasePage.find_elements(self, self.page_elements.CONTAINER_LIST_RESULT_SMARTPHONES)
        
        for smartphone in container_list_smartphones:
            element = smartphone.find_element(self.By.TAG_NAME, "h1")
            if element.text == model:
                element.click()
                break
        
        
