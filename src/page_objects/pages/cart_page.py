from src.page_objects.page_elements.cart_page_elements import ShoppingCartFlow
from src.page_objects.page_elements.cart_page_elements import CartPageElement

class CartPage:
    
    def __init__(self, driver):
        self.driver = driver
        self.page_elements = CartPageElement()
        self.shopping_cart_flow = ShoppingCartFlow(self.driver, self.page_elements)

