from behave import *
from hamcrest import *
from src.page_objects.pages.faq_page import FAQPage


@given('I want access the "{link}" page')
def step_impl(context, link):
    
    context.link = link


@when('I perfom the faq delivery flow')
def step_impl(context):
    
    context.faq_page = FAQPage(context.driver)
    context.faq_page.faq_flow.access_faq(context.link)
    
    
@then('I print the third question presented and the answer of this question')
def step_impl(context):
    
    context.faq_page.faq_flow.print_question_answer()