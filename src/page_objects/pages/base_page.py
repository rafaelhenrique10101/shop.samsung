from src.page_objects.selenium_actions import SeleniumActions
import time


class BasePage(SeleniumActions):

    def __init__(self, driver):
        
        self.driver = driver
        
        
    def open(self, address):
        self.open_url(address)
        
        
    def get_url(self):
        self.get_current_url()
        
        
    def get_page_title(self):
        self.get_title()
