from behave import *
from hamcrest import *
from src.page_objects.pages.smartphone_page import SmartphonePage


@when('I change the sort option to "{sort}"')
def step_impl(context, sort):
    
    context.smartphone_page = SmartphonePage(context.driver)
    context.smartphone_page.sort.change_sort_options(sort)


@when('I scroll the page until the end')
def step_impl(context):
    
    context.smartphones = context.smartphone_page.smartphone_list.get_smartphones()
    
    
@then('I print the name of "{qtd}" smarthphones in console')
def step_impl(context, qtd):
        
    context.smartphone_page.smartphone_list.print_smartphone_console(context.smartphones)
    
    assert_that(len(context.smartphones), int(qtd), f"Quantity of smartphones is wrong, must be {qtd}")



