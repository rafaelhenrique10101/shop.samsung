from behave import *
from hamcrest import *
from src.page_objects.pages.header_page import HeaderPage
from src.page_objects.pages.search_page import SearchPage
from src.page_objects.pages.cart_page import CartPage
from src.page_objects.pages.checkout_page import CheckoutPage
import time

@given('I have the personal data')
def step_impl(context):
    
    for row in context.table:
        context.email = row['email']
        context.first_name = row['first_name']
        context.last_name = row['last_name']
        context.cpf = row['cpf']
        context.phone = row['phone']
        
        
@given('I have the delivery data')
def step_impl(context):
    
    for row in context.table:
        context.postal_code = row['postal_code']
        context.number = row['number']
        
        
        
@when('I perform a search by smartphone "{model}"')
def step_impl(context, model):
    
    context.model = model
    context.header_page = HeaderPage(context.driver)
    context.header_page.header_menu.search_by_model(model)
    
    
@when('I select the smartphone in list')
def step_impl(context):
    
    context.search_page = SearchPage(context.driver)
    context.search_page.list_results.select_smartphone(context.model)


@when('I proceed with cart flow')
def step_impl(context):
    
    context.cart_page = CartPage(context.driver)
    context.cart_page.shopping_cart_flow.cart_flow(context.postal_code)
    
    
@when('I inform the email data to continue with checkout flow')
def step_impl(context):
    
    context.checkout_page = CheckoutPage(context.driver)
    context.checkout_page.checkout_email_flow.fill_email(context.email)
    
    
@when('I fill the personal data to continue with delivery flow')
def step_impl(context):
        
    context.checkout_page.checkout_personal_data_flow.fill_personal_data(context.first_name,
                                                                         context.last_name,
                                                                         context.cpf,
                                                                         context.phone)
    
    
@when('I fill the delivery data to continue with payment flow')
def step_impl(context):
        
    context.checkout_page.checkout_shipping_flow.fill_shipping_data(context.number)
    
    
@then('I see in the order summary the ship value as "{ship}"')
def step_impl(context, ship):
    
    response = context.checkout_page.checkout_validate_shipping.get_shipping()
    assert_that(response, ship, "The method shipping is not 'Grátis'")
    time.sleep(2)
