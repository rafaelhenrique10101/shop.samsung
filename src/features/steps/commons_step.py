from behave import *
from hamcrest import *
from src.page_objects.pages.base_page import BasePage
from src.page_objects.pages.header_page import HeaderPage


@given('I am on the "{page}" page')
def step_impl(context, page):
    context.base_page = BasePage(context.driver)
    context.base_page.open(context.base_url + page)


@when('I click in "{link}" link')
def step_impl(context, link):
    context.header_page = HeaderPage(context.driver)
    context.header_page.header_menu.access_smartphone_menu(link)
