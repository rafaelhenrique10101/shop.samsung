from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By


class SeleniumActions():
    
    By = By()
            
    def find_element(self, locator):
        return self.wait_for_element(expected_conditions.visibility_of_element_located(locator))


    def find_elements(self, locator):
        return self.wait_for_element(expected_conditions.visibility_of_all_elements_located(locator))
    
    
    def find_link(self, link_text):
        return self.driver.find_element_by_link_text(link_text)
    
    
    def open_url(self, url):
        return self.driver.get(url)
    
    
    def get_current_url(self):
        return self.driver.current_url
    
    
    def get_title(self):
        return self.driver.title
    
    
    def wait_for_element(self, state_el):
        return WebDriverWait(self.driver, 30).until(state_el)
    
    
    def click(self, locator):
        return self.wait_for_element(expected_conditions.element_to_be_clickable(locator)).click()
    
    
    def set_text(self, locator, text):
        el = self.find_element(locator)
        el.clear()
        el.send_keys(text)
        
        
    def page_down(self, locator):
        el = self.find_element(locator)
        el.send_keys(Keys.PAGE_DOWN)
        
        
    def page_end(self, locator):
        el = self.find_element(locator)
        el.send_keys(Keys.END)
        
        
    def switch_iframe(self, iframe):
        self.driver.switch_to.frame('neoassist-widget-frame-2')
        
