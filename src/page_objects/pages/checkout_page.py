from src.page_objects.page_elements.checkout_page_elements import CheckoutEmailFlow
from src.page_objects.page_elements.checkout_page_elements import CheckoutPersonalDataFlow
from src.page_objects.page_elements.checkout_page_elements import CheckoutShippingFlow
from src.page_objects.page_elements.checkout_page_elements import CheckoutValidateShipping
from src.page_objects.page_elements.checkout_page_elements import CheckoutPageElement

class CheckoutPage:
    
    def __init__(self, driver):
        self.driver = driver
        self.page_elements = CheckoutPageElement()
        self.checkout_email_flow = CheckoutEmailFlow(self.driver, self.page_elements)
        self.checkout_personal_data_flow = CheckoutPersonalDataFlow(self.driver, self.page_elements)
        self.checkout_shipping_flow = CheckoutShippingFlow(self.driver, self.page_elements)
        self.checkout_validate_shipping = CheckoutValidateShipping(self.driver, self.page_elements)

