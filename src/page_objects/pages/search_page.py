from src.page_objects.page_elements.search_page_elements import ListResultSmartphones
from src.page_objects.page_elements.search_page_elements import SearchPageElement

class SearchPage:
    
    def __init__(self, driver):
        self.driver = driver
        self.page_elements = SearchPageElement()
        self.list_results = ListResultSmartphones(self.driver, self.page_elements)
