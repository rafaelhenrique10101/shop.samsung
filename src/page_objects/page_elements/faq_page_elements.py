from src.page_objects.pages.base_page import BasePage
from time import sleep


class FAQPageElement(BasePage):
    
    def __init__(self):
                
        self.BTN_DELIVERY = (self.By.XPATH, "/html/body/div[1]/div[2]/div/div/div[3]")
        self.CONTAINER_QUESTIONS_ANSWERS = (self.By.XPATH, "/html/body/div[1]/div[2]/div/div[2]/ul")
        self.BODY = (self.By.TAG_NAME, "body")
        self.IFRAME_DELIVERY = "neoassist-widget-frame-2"
        
  
        
class FAQFlow(FAQPageElement):
        
    def __init__(self, driver, page_elements):
        self.driver = driver
        self.page_elements = page_elements
        
        
    def access_faq(self, label_link):
        sleep(8)
        BasePage.page_end(self, self.page_elements.BODY)
        BasePage.find_link(self, label_link).click()
        BasePage.switch_iframe(self, self.page_elements.IFRAME_DELIVERY)
        BasePage.click(self, self.page_elements.BTN_DELIVERY)
        
        
    def print_question_answer(self):
        
        container_questions_answers = BasePage.find_element(self, self.page_elements.CONTAINER_QUESTIONS_ANSWERS)
        questions = container_questions_answers.find_elements(self.By.TAG_NAME, "li")
        
        for key, question in enumerate(questions):
            if key == 2:
                question_text = question.find_element(self.By.CSS_SELECTOR, "a.question-container span.title").text
                print("===================================================================================================")
                print(question_text)
                question.find_element(self.By.TAG_NAME, "a").click()
                answer_text = question.find_element(self.By.CSS_SELECTOR, "div.answer div span").text
                print(answer_text)
                print("===================================================================================================")
        
        