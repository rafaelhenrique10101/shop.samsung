from src.page_objects.pages.base_page import BasePage
from time import sleep


class CheckoutPageElement(BasePage):
    
    def __init__(self):
                
        self.EMAIL = (self.By.ID, "client-pre-email")
        self.BTN_CONTINUE = (self.By.ID, "btn-client-pre-email")
        self.INPUT_FIRST_NAME = (self.By.ID, "client-first-name")
        self.INPUT_LAST_NAME = (self.By.ID, "client-last-name")
        self.INPUT_CPF = (self.By.ID, "client-document")
        self.INPUT_PHONE = (self.By.ID, "client-phone")
        self.BTN_GO_TO_SHIPPING = (self.By.ID, "go-to-shipping")
        self.INPUT_SHIP_NUMBER = (self.By.ID, "ship-number")
        self.BTN_GO_TO_PAYMENT = (self.By.ID, "btn-go-to-payment")
        self.CONTAINER_CART_TOTALIZERS = (self.By.XPATH, "/html/body/div[5]/div[3]/div[2]/div[2]/div/div[2]/div/div[3]")
        
  
        
class CheckoutEmailFlow(CheckoutPageElement):
        
    def __init__(self, driver, page_elements):
        self.driver = driver
        self.page_elements = page_elements
        
        
    def fill_email(self, email):
        
        BasePage.set_text(self, self.page_elements.EMAIL, email)
        BasePage.click(self, self.page_elements.BTN_CONTINUE)
        
        
class CheckoutPersonalDataFlow(CheckoutPageElement):
        
    def __init__(self, driver, page_elements):
        self.driver = driver
        self.page_elements = page_elements
        
        
    def fill_personal_data(self, first_name, last_name, cpf, phone):
        
        BasePage.set_text(self, self.page_elements.INPUT_FIRST_NAME, first_name)
        BasePage.set_text(self, self.page_elements.INPUT_LAST_NAME, last_name)
        BasePage.set_text(self, self.page_elements.INPUT_CPF, cpf)
        BasePage.set_text(self, self.page_elements.INPUT_PHONE, phone)
        BasePage.click(self, self.page_elements.BTN_GO_TO_SHIPPING)
        
        
class CheckoutShippingFlow(CheckoutPageElement):
        
    def __init__(self, driver, page_elements):
        self.driver = driver
        self.page_elements = page_elements
        
        
    def fill_shipping_data(self, number):
        
        sleep(3)
        BasePage.set_text(self, self.page_elements.INPUT_SHIP_NUMBER, number)
        sleep(1)
        BasePage.page_down(self, self.page_elements.INPUT_SHIP_NUMBER)
        sleep(2)
        BasePage.click(self, self.page_elements.BTN_GO_TO_PAYMENT)
        sleep(3)
        
        
class CheckoutValidateShipping(CheckoutPageElement):
        
    def __init__(self, driver, page_elements):
        self.driver = driver
        self.page_elements = page_elements
        
        
    def get_shipping(self):
        
        container_cart_totalizers = BasePage.find_element(self, self.page_elements.CONTAINER_CART_TOTALIZERS)
        ship_mode = container_cart_totalizers.find_element(self.By.CSS_SELECTOR, "tr.srp-summary-result td.monetary").text
        
        return ship_mode
        
        
