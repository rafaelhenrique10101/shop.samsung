from src.page_objects.pages.base_page import BasePage
from time import sleep


class CartPageElement(BasePage):
    
    def __init__(self):
                
        self.BTN_BUY = (self.By.XPATH, "/html/body/div[2]/div/div[1]/div/div/div/div[2]/div/div[4]/div/div[1]/div/div/div[3]/button/div")
        self.BTN_POSTAL_CODE_CALCULATE = (self.By.ID, "shipping-calculate-link")
        self.INPUT_POSTAL_CODE = (self.By.ID, "ship-postalCode")
        self.BTN_CLOSE_ORDER = (self.By.ID, "cart-to-orderform")
  
        
class ShoppingCartFlow(CartPageElement):
        
    def __init__(self, driver, page_elements):
        self.driver = driver
        self.page_elements = page_elements
        
        
    def cart_flow(self, postal_code):
        BasePage.click(self, self.page_elements.BTN_BUY)
        sleep(10)
        BasePage.click(self, self.page_elements.BTN_POSTAL_CODE_CALCULATE)
        BasePage.set_text(self, self.page_elements.INPUT_POSTAL_CODE, postal_code)
        sleep(5)
        BasePage.click(self, self.page_elements.BTN_CLOSE_ORDER)
        
