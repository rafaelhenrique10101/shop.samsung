from src.page_objects.pages.base_page import BasePage


class SmartphonePageElement(BasePage):
    
    def __init__(self):
        
        self.SORT_BUTTON = (self.By.CLASS_NAME, "vtex-search-result-3-x-orderByButton")
        self.CONTAINER_ITEMS_SORT = (self.By.CLASS_NAME, "vtex-search-result-3-x-orderByOptionsContainer")
        self.CONTAINER_SMARTPHONES = (self.By.XPATH, "/html/body/div[2]/div/div[1]/div/div[2]/div/div[2]/section/div[2]/div/div/div/div[5]/div[3]")
        self.TOTAL_SMARTPHONES = (self.By.CSS_SELECTOR, "div.acupula-samsung-store-0-x-totalProducts b")
        self.BODY = (self.By.TAG_NAME, "body")

    
class Sort(SmartphonePageElement):
    
    def __init__(self, driver, page_elements):
        self.driver = driver
        self.page_elements = page_elements
            
            
    def change_sort_options(self, sort):
        BasePage.click(self, self.page_elements.SORT_BUTTON)
        self.select_sort(sort)
        
        
    def select_sort(self, sort):
        
        container_element = BasePage.find_element(self, self.page_elements.CONTAINER_ITEMS_SORT)
        items = container_element.find_elements(self.By.TAG_NAME, "button")
        
        for item in items:
            if item.text == sort:
                item.click()
                break
        


class SmartphoneList(SmartphonePageElement):
    
    def __init__(self, driver, page_elements):
        self.driver = driver
        self.page_elements = page_elements
        
        
    def get_smartphones(self):
        
        qtd_total_smartphones = BasePage.find_element(self, self.page_elements.TOTAL_SMARTPHONES).text
        container_smartphones = BasePage.find_element(self, self.page_elements.CONTAINER_SMARTPHONES)
        smartphones = container_smartphones.find_elements(self.page_elements.By.CSS_SELECTOR, "div.vtex-search-result-3-x-galleryItem")
        
        while len(smartphones) < int(qtd_total_smartphones):
            BasePage.page_down(self, self.page_elements.BODY)
            smartphones = container_smartphones.find_elements(self.By.CSS_SELECTOR, "div.vtex-search-result-3-x-galleryItem")
        
        
        return smartphones
    
    
    def print_smartphone_console(self, smartphones):
        
        print("")
        print("====================================================")
        
        for key, smartphone in enumerate(smartphones):
            name = smartphone.find_element(self.By.TAG_NAME, "h3").text
            key += 1
            print(f"{key} - " + name)
            
        print("====================================================")
