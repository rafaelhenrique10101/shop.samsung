Feature: Task 2

    @print_smartphones_name
    Scenario: Print the smartphones name
        Given I am on the "/" page
        When I click in "Telefonia" link
        And I click in "Smartphones" link
        And I change the sort option to "Menor preço"
        And I scroll the page until the end
        Then I print the name of "20" smarthphones in console


    @purchase_flow_until_defining_payment_information
    Scenario: Print the smartphones name
        Given I am on the "/" page
        And I have the personal data
            |email                      |first_name |last_name    |cpf            |phone        |
            |italo.valenca@example.com  |Italo      |Valenca      |32927734070    |81999668877  |

        And I have the delivery data
            |postal_code |number |
            |01310924    |1195   |

        When I perform a search by smartphone "Galaxy A51"
        And I select the smartphone in list
        And I proceed with cart flow
        And I inform the email data to continue with checkout flow
        And I fill the personal data to continue with delivery flow
        And I fill the delivery data to continue with payment flow
        Then I see in the order summary the ship value as "Grátis"


    @print_ship_information_available_in_the_FAQ @debug
    Scenario: Print the third question presented and the answer of this question
        Given I am on the "/" page
        And I want access the "FAQ Loja Online" page
        When I perfom the faq delivery flow
        Then I print the third question presented and the answer of this question