from src.page_objects.page_elements.header_page_elements import HeaderMenu
from src.page_objects.page_elements.header_page_elements import HeaderPageElement

class HeaderPage:
    
    def __init__(self, driver):
        self.driver = driver
        self.page_elements = HeaderPageElement()
        self.header_menu = HeaderMenu(self.driver, self.page_elements)
